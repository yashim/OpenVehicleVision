# OpenVV: Open Source Vehicle Vision Library

## Road Map

    looooong way to go...

Road Detection

- [x] Lane Marking Detection
- [ ] Lane Marking Recognition
- [ ] Special Road Marks Detection
- [ ] Traffic Sign Recognition
- [ ] Traffic Light Recognition

Road Scene Object Detection

- [x] Vehicle Detection
- [x] Vehicle Recognition
- [x] Pedestrian Detection

## Resources

Related dataset, more detection results can be found and download free [here](http://pan.baidu.com/s/1eQ8V2aY#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision)

## Document

* Check code comments via matlab cmd `help foreach_file_do` or `help FilePick.one` 
* View [online doc](https://github.com/baidut/OpenVehicleVision/issues?q=is%3Aissue+is%3Aclosed+label%3Adocument) 
* Comment if you have any confuse
* If your required document is not found, please add one issue [here](https://github.com/baidut/OpenVehicleVision/issues/new)

## Publications

**Robust Lane Marking Detection using Boundary-Based Inverse Perspective Mapping**
    
*Zhenqiang Ying & Ge Li In [ICASSP 2016](http://www.icassp2016.org/)*

* paper:[pdf](http://pan.baidu.com/s/1eQ8V2aY/#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision%252Ficassp2016)
* poster: [SigPort](http://sigport.org/998) 
* code: main algorithm in `main/roadDetectionViaBird.m`, runtest in `ICASSP2016.m` 
* related materials are available in [baiduyun](http://pan.baidu.com/s/1eQ8V2aY/#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision%252Ficassp2016)
* [more detection results](http://pan.baidu.com/s/1eQ8V2aY/#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision%252Ficassp2016%252Fdetection%2520results(SLD2011))

---

**An Illumination-Robust Approach for Feature-Based Road Detection**

*Zhenqiang Ying, Ge Li & Guozhen Tan In [ISM 2015](http://www.ieeeism.com/)*

* paper:[html](http://baidut.github.io/publication/documents/ism2015/zqying_ism2015_paper.html)
* code:[branch](https://github.com/baidut/OpenVehicleVision/tree/ISM2015) | [release](https://github.com/baidut/OpenVehicleVision/releases/tag/ism2015) 
* related materials are available in [baiduyun](http://pan.baidu.com/s/1eQ8V2aY#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision%252Fism2015)
* [more detection results](http://pan.baidu.com/s/1eQ8V2aY/#path=%252F%25E9%25A1%25B9%25E7%259B%25AE%25E5%2585%25AC%25E5%25BC%2580%252FOpenVehicleVision%252Fism2015&render-type=grid-view)

## License

MIT
